# animateFont
This jQuery plugin will simply toggle CSS classes of a given word or sentence letter by letter.

## Init 
The jQuery plugin has to be initialized inside the jQuery ready function. It is required to define the CSS classes you want to use. Therefore you should add the CSSClassPool array.
```javascript
$(document).ready(function() {
    
    $('#fontContainer').animateFont({
        CSSClassPool: [
            'animateFont-sans', 
            'animateFont-serif', 
            'animateFont-monospace'
        ]
    });

}); 
```

## HTML 
The HTML part is very simple:
```
<div id="fontContainer">Bit&amp;Black</div>
```

## CSS 
The CSS part could look like that:
```
#fontContainer {
    font-size: 4em;
    line-height: 1;
}

.animateFont-sans {
    font-family: Helvetica, Arial, sans-serif;
}

.animateFont-serif {
    font-family: Georgia, serif;
}

.animateFont-monospace {
    font-family: 'Courier New', Courier, monospace;
}
```

## Additional parameters
There are three more options you can set when initializing the plugin:
* ```timeAnimation``` How fast the animation should run (in milliseconds)
* ```timeAfterAnimation``` How long the animation should wait after changing the classes in every letter (in milliseconds)
* ```timeAfterAll``` How long the animation should wait after changing all the classes of the CSSClassPool array once (in milliseconds)
