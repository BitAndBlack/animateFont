// Bit&Black 2017

(function($) {
    
    $.fn.animateFont = function(settings) {
    
        var instances = {};
        
    
        // Keep chain
    
        return this.each(function() {
            
            if(!instances[$(this)]){
                instances[$(this)] = animateFont($(this), settings);
            }                        
        });

        
        function animateFont(element, settings) {
            
                
            // Settings
            
            var classBefore = 0;
            var classCurrent = 1;
            var charCounter = 0;
            
            var fontContainer, text, length, timeout; 
            
            var options = {
                CSSClassPool: [], 
                timeAnimation: 50,
                timeAfterAnimation: 500,
                timeAfterAll: 2000
            };
            
            
            // Init 
    
            init(element, settings);
    
    
            // Init elements and settings
        
            function init(element, settings) {
                                
                fontContainer = $(element);
                
                text = $(fontContainer).text(); 
                
                length = text.length;
                
                options = $.extend({}, options, settings);

                $(fontContainer).text('');
                
                for (i = 0; i < length; i++) {
                    
                    var span = $('<span></span>');
                    span.text(text.charAt(i));
                    span.addClass(options.CSSClassPool[0]);
                    
                    $(fontContainer).append(span);
                }
                
                if (options.CSSClassPool.length == 0) {
                    console.error('You need to add some css classes.');
                }
            
                animateFont();
            }
        
        
            // Start animation
        
            function animateFont() { 
                        
                $(fontContainer)
                    .find('span') 
                    .eq(charCounter)
                    .removeClass(options.CSSClassPool[classBefore])
                    .addClass(options.CSSClassPool[classCurrent]); 
                        
                if (charCounter == length) {
                    charCounter = 0;
                    
                    if (classCurrent == options.CSSClassPool.length - 1) {
                        classCurrent = 0;                
            
                        clearTimeout(timeout);
                        timeout = setTimeout(animateFont, options.timeAfterAll);
                    }
                    else {
                        classCurrent++;
            
                        clearTimeout(timeout);
                        timeout = setTimeout(animateFont, options.timeAfterAnimation);
                    }
            
                    if (classBefore == options.CSSClassPool.length - 1) {
                        classBefore = 0;
                    }
                    else {
                        classBefore++;
                    }
            
                }
                else {
                    charCounter++;
                    clearTimeout(timeout);
                    timeout = setTimeout(animateFont, options.timeAnimation);
                }
            }
            
        }

    }

}(jQuery));